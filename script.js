window.onload = maingame
function maingame () {
  var game = {};
  game.money = 0;
  game.name = 'Tha best';
  game.cps = 0;
  game.buildings = 0;
  game.clickValue = 1;
  game.clickUpgrade = 200;
  game.clickLevel = 0;
  game.clickCount = 0;
  game.mouseCount = 0;
  game.unit1 = new Unit(50, 4, 0);
  game.unit2 = new Unit(5000, 24, 0);
  game.unit3 = new Unit(51000, 144, 0);
  game.unit4 = new Unit(520000, 864, 0);
  game.unit5 = new Unit(5300000, 5180, 0);
  game.unit6 = new Unit(54100000, 31100, 0);
  game.unit7 = new Unit(358000000, 186000, 0);
  game.unit8 = new Unit(2350000000, 1110000, 0);
  game.unit9 = new Unit(15300000000, 6710000, 0);
  game.unit10 = new Unit(99700000000, 40300000, 0);
  game.unit11 = new Unit(642000000000, 24100000, 0);
  game.unit12 = new Unit(4120000000000, 1450000000, 0);
  game.unit13 = new Unit(26300000000000, 8700000000, 0);
  game.unit14 = new Unit(167000000000000, 52200000000, 0);

  var gameSave = '';
  var clickerImg = document.querySelector('#clicker');
  var clickerUpgr = document.querySelector('#upgrClickValue');
  var inputName = document.querySelector('.input-btn');
  var unitUpgr1 = document.querySelector('#unit1');
  var unitUpgr2 = document.querySelector('#unit2');
  var unitUpgr3 = document.querySelector('#unit3');
  var unitUpgr4 = document.querySelector('#unit4');
  var unitUpgr5 = document.querySelector('#unit5');
  var unitUpgr6 = document.querySelector('#unit6');
  var unitUpgr7 = document.querySelector('#unit7');
  var unitUpgr8 = document.querySelector('#unit8');
  var unitUpgr9 = document.querySelector('#unit9');
  var unitUpgr10 = document.querySelector('#unit10');
  var unitUpgr11 = document.querySelector('#unit11');
  var unitUpgr12 = document.querySelector('#unit12');
  var unitUpgr13 = document.querySelector('#unit13');
  var unitUpgr14 = document.querySelector('#unit14');
  var achievmentsNav = document.querySelector('#achievments-nav');
  var achievmentsContainer = document.querySelector('#achievments').querySelector('div');
  var resetbtn = document.querySelector('.reset-game');
  var aboutNav = document.querySelector('#about-nav');
  var aboutContainer = document.querySelector('#about').querySelector('div');
  var upgradeNav = document.querySelector('#upgrades-nav');
  var upgradesContainer = document.querySelector('#upgrades').querySelector('div');
  var mainNav = document.querySelector('#main-nav');
  var mainContainer = document.querySelector('#maingame').querySelector('div');

  clearNotifications();
  load();
  update();
  updateHtmlDom();
  randomCoin();
  eventlisteners();

  function add (obj) {
    if (game.money >= obj.cost) {
      game.money = game.money - obj.cost;
      obj.owned++;
      game.cps += obj.cps;
      obj.cost *= 1.28;
      game.buildings++;
    } else {
      document.querySelector('.ntf').innerHTML = 'Not enough money to buy upgrade '
    }
  }

  function clicksPerSecond () {
    var count = 0;
    var numSec = 1;
    var start = 0;
    clickerImg.addEventListener('click', function () {
      count++;
      start++;
    });
    getCPS();
    function getCPS () {
      setTimeout(function () {
        document.querySelector('.cps').innerHTML = 'Clicks earning =' + count * game.clickValue + '$';
        count = 0;
        getCPS()
      }, numSec * 1000)
    }
  }

  function clickerMain () {
    game.money += game.clickValue;
    document.querySelector('.money').innerHTML = 'Money =' + game.money.toFixed(2) + '$';
    document.querySelector('.moneyfooter').innerHTML = 'Money =' + game.money.toFixed(2) + '$';
    game.clickCount++;
    game.mouseCount++;
  }

  function clearNotifications () {
    setInterval(function () {
      document.querySelector('.ntf').innerHTML = ''
    }, 20000)
  }

  function eventlisteners () {
    achievmentsNav.addEventListener('click', function () {
      mainContainer.style.display = 'none';
      upgradesContainer.style.display = 'none';
      achievmentsContainer.style.display = 'block';
      aboutContainer.style.display = 'none';
    });
    aboutNav.addEventListener('click', function () {
      mainContainer.style.display = 'none';
      upgradesContainer.style.display = 'none';
      achievmentsContainer.style.display = 'none';
      aboutContainer.style.display = 'block';
    });
    clickerImg.addEventListener('click', clickerMain);
    clickerImg.addEventListener('click', clicksPerSecond);
    clickerUpgr.addEventListener('click', upgradeClicker);
    mainNav.addEventListener('click', function () {
      mainContainer.style.display = 'block'
      upgradesContainer.style.display = 'none'
      achievmentsContainer.style.display = 'none'
      aboutContainer.style.display = 'none'
    });
    inputName.addEventListener('click', gameName);
    resetbtn.addEventListener('click', resetGame);
    upgradeNav.addEventListener('click', function () {
      mainContainer.style.display = 'none'
      upgradesContainer.style.display = 'block';
      achievmentsContainer.style.display = 'none';
      aboutContainer.style.display = 'none'
    });
    unitUpgr1.addEventListener('click', function () {
      add(game.unit1);
      updateHtmlDom()
    });
    unitUpgr2.addEventListener('click', function () {
      add(game.unit2);
      updateHtmlDom()
    });
    unitUpgr3.addEventListener('click', function () {
      add(game.unit3);
      updateHtmlDom()
    });
    unitUpgr4.addEventListener('click', function () {
      add(game.unit4);
      updateHtmlDom();
    });
    unitUpgr5.addEventListener('click', function () {
      add(game.unit5);
      updateHtmlDom()
    });
    unitUpgr6.addEventListener('click', function () {
      add(game.unit6);
      updateHtmlDom()
    });
    unitUpgr7.addEventListener('click', function () {
      add(game.unit7);
      updateHtmlDom()
    });
    unitUpgr8.addEventListener('click', function () {
      add(game.unit8);
      updateHtmlDom()
    });
    unitUpgr9.addEventListener('click', function () {
      add(game.unit9);
      updateHtmlDom()
    });
    unitUpgr10.addEventListener('click', function () {
      add(game.unit10);
      updateHtmlDom()
    });
    unitUpgr11.addEventListener('click', function () {
      add(game.unit11);
      updateHtmlDom()
    });
    unitUpgr12.addEventListener('click', function () {
      add(game.unit12);
      updateHtmlDom()
    });
    unitUpgr13.addEventListener('click', function () {
      add(game.unit13);
      updateHtmlDom()
    });
    unitUpgr14.addEventListener('click', function () {
      add(game.unit14);
      updateHtmlDom()
    })
  }

  function upgradeClicker () {
    if (game.money >= game.clickUpgrade) {
      game.money = game.money - game.clickUpgrade;
      game.clickValue = game.clickValue * 2;
      game.clickUpgrade = game.clickUpgrade * 3;
      game.clickLevel++;
      updateHtmlDom()
    } else {
      document.querySelector('.ntf').innerHTML = 'Not enought money to buy upgrade '
    }
  }

  function moneyPerSecond () {
    game.money = game.money + game.cps
  }

  function load () {
    if (localStorage.getItem('idlegame') !== null) {
      gameSave = localStorage.getItem('idlegame');
      game = JSON.parse(gameSave)
    }
  }

  function update () {
    setInterval(function () {
      save();
      moneyPerSecond();
      updateHtmlDom()
    }, 1000)
  }

  function updateHtmlDom () {
    document.querySelector('.upgrClickValue').innerHTML = 'Cost = ' + game.clickUpgrade + '$ ClickValue = ' + game.clickValue + ' Level =' + game.clickLevel;
    document.querySelector('.unit1').innerHTML = 'Cost = ' + game.unit1.cost.toFixed(0) + '$ Money/s = ' + game.unit1.cps * game.unit1.owned + '$ owned =' + game.unit1.owned;
    document.querySelector('.unit2').innerHTML = 'Cost = ' + game.unit2.cost.toFixed(0) + '$ Money/s = ' + game.unit2.cps * game.unit2.owned + '$ owned =' + game.unit2.owned;
    document.querySelector('.unit3').innerHTML = 'Cost = ' + game.unit3.cost.toFixed(0) / 1000 + ' Thousand $ Money/s = ' + game.unit3.cps * game.unit3.owned + '$ owned =' + game.unit3.owned;
    document.querySelector('.unit4').innerHTML = 'Cost = ' + game.unit4.cost.toFixed(2) / 1000 + ' Thousand $ Money/s = ' + game.unit4.cps * game.unit4.owned + '$ owned =' + game.unit4.owned;
    document.querySelector('.unit5').innerHTML = 'Cost = ' + game.unit5.cost.toFixed(2) / 1000000 + ' Million $ Money/s = ' + game.unit5.cps * game.unit5.owned + '$ owned =' + game.unit5.owned;
    document.querySelector('.unit6').innerHTML = 'Cost = ' + game.unit6.cost.toFixed(0) / 1000000 + ' Million $ Money/s = ' + game.unit6.cps * game.unit6.owned + '$ owned =' + game.unit6.owned;
    document.querySelector('.unit7').innerHTML = 'Cost = ' + game.unit7.cost.toFixed(0) / 1000000 + ' Million $ Money/s = ' + game.unit7.cps * game.unit7.owned + '$ owned =' + game.unit7.owned;
    document.querySelector('.unit8').innerHTML = 'Cost = ' + game.unit8.cost.toFixed(0) / 1000000000 + ' Billion $ Money/s = ' + game.unit8.cps * game.unit8.owned + '$ owned =' + game.unit8.owned;
    document.querySelector('.unit9').innerHTML = 'Cost = ' + game.unit9.cost.toFixed(0) / 1000000000 + ' Billion $ Money/s = ' + game.unit9.cps * game.unit9.owned + '$ owned =' + game.unit9.owned;
    document.querySelector('.unit10').innerHTML = 'Cost = ' + game.unit10.cost.toFixed(0) / 1000000000 + ' Billion $ Money/s = ' + game.unit10.cps * game.unit10.owned + '$ owned =' + game.unit10.owned;
    document.querySelector('.unit11').innerHTML = 'Cost = ' + game.unit11.cost.toFixed(0) / 1000000000 + ' Billion $ Money/s = ' + game.unit11.cps * game.unit11.owned + '$ owned =' + game.unit11.owned;
    document.querySelector('.unit12').innerHTML = 'Cost = ' + game.unit12.cost.toFixed(0) / 1000000000000 + ' Trillion $ Money/s = ' + game.unit12.cps * game.unit12.owned + '$ owned =' + game.unit12.owned;
    document.querySelector('.unit13').innerHTML = 'Cost = ' + game.unit13.cost.toFixed(0) / 1000000000000 + ' Trillion $ Money/s = ' + game.unit13.cps * game.unit13.owned + '$ owned =' + game.unit13.owned;
    document.querySelector('.unit14').innerHTML = 'Cost = ' + game.unit14.cost.toFixed(0) / 1000000000000 + ' Trillion $ Money/s = ' + game.unit14.cps * game.unit14.owned + '$ owned =' + game.unit14.owned;
    document.querySelector('.money').innerHTML = 'Money =' + game.money.toFixed(2) + '$';
    document.querySelector('.moneyfooter').innerHTML = 'Money =' + game.money.toFixed(2) + '$';
    document.querySelector('.buildings').innerHTML = 'Total buildings =' + game.buildings;
    document.querySelector('.totalCps').innerHTML = 'Cash per second = ' + game.cps + '$';
    document.querySelector('.mouseClicks').innerHTML = 'Mouse Total clicks = ' + game.mouseCount;
    document.querySelector('.name').innerHTML = 'This is ' + game.name + ' Kingdom'
  }

  function save () {
    gameSave = JSON.stringify(game);
    localStorage.setItem('idlegame', gameSave);
  }

  function randomCoin () {
    var multiplier = [8,10,20,68,45,25,78,533,100,200,400,1000,1,5,9,7,5,6,7,8,9,22,44,55,88,777,66,55,99,885,546,874,4568,10000]
    var time = Math.floor((Math.random() * 10000) + 10);
    var width = window.innerWidth;
    var height = window.innerHeight;
    var posx = (Math.round(Math.random() * width) + 10) - 50;
    var posy = (Math.round(Math.random() * height) + 10) - 50;
    var index = (Math.floor((Math.random() * multiplier.length - 1) + 1));
    var coins = document.querySelector('.main-game');
    var coin = document.createElement('div');
    coin.className = 'coin';
    coin.style.top = posy + 'px';
    coin.style.left = posx + 'px';

    setTimeout(function () {
      setTimeout(function () {
        coins.appendChild(coin);
        var timer = 10;
        document.querySelector('.coin').addEventListener('click', function () {
          if (game.cps === 0) {
            game.money = game.money + (0.5 * multiplier[index]);
            coins.removeChild(coin);
            randomCoin();
            document.querySelector('.ntf').innerHTML = 'Congratulations you found A Gold coin<br>multiplier =' +
                            multiplier[index] + 'x <br>' + 'money gained = ' + 0.5 * multiplier[index] + ' $';
            updateHtmlDom()
          } else {
            game.money = game.money + (game.cps * multiplier[index]);
            coins.removeChild(coin);
            randomCoin();
            document.querySelector('.ntf').innerHTML = 'Congratulations you Found A Gold coin <br> Coin multiplier =' +
                            multiplier[index] + ' X <br>' + 'money gained = ' + game.cps * multiplier[index] + ' $';
            updateHtmlDom()
          }
        });
        var coinTime = setInterval(function () {
          timer--;
          if (timer === 0) {
            if (document.querySelector('.coin')) {
              coins.removeChild(coin);
              randomCoin();
              clearTimeout(coinTime)
            }
          }
        }, 1000)
      }, 10000)
    }, time)
  }

  function Unit (cost, cps, owned) {
    this.cost = cost;
    this.cps = cps;
    this.owned = owned;

  }

  function gameName () {
    game.name = document.querySelector('.input-text').value;
    document.querySelector('.name').innerHTML = 'This is ' + game.name + ' Kingdom'
    document.querySelector('.input-text').value = ''
  }

  function resetGame () {
    localStorage.removeItem('idlegame');
    location.reload()
  }
}
